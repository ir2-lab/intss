PK     Ë¥fNñBH         mimetypetext/x-wxmathmlPK     Ë¥fNQdBV5  5  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     Ë¥fN³`       content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 19.03.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="2">

<cell type="code">
<input>
<editor type="input">
<line>F(y0,y1,y2,y3,y4,y5):=[-f*exp(-y5)-b0*y0*y1,</line>
<line>    -f*exp(-y5)-b0*y0*y1-2*b1*y1^2-b2*y2*y1-b4*y4*y1,</line>
<line>b1*y1^2-b2*y2*y1,</line>
<line>b2*y2*y1,</line>
<line>-b4*y4*y1*st,</line>
<line>b0*(y0+y1)+2*b1*y1+b2*y2+b4*y4];</line>
</editor>
</input>
<output>
<mth><lbl>(%o1) </lbl><fn><r><fnm>F</fnm></r><r><p><v>y0</v><t>,</t><v>y1</v><t>,</t><v>y2</v><t>,</t><v>y3</v><t>,</t><v>y4</v><t>,</t><v>y5</v></p></r></fn><t>:=</t><t>[</t><r><p><v>â</v><v>f</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y5</v></p></r></fn><v>â</v><v>b0</v><h>*</h><v>y0</v><h>*</h><v>y1</v><t>,</t><r><p><v>â</v><v>f</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y5</v></p></r></fn><v>â</v><v>b0</v><h>*</h><v>y0</v><h>*</h><v>y1</v><v>+</v><r><p><v>â</v><n>2</n></p></r><h>*</h><v>b1</v><h>*</h><e><r><v>y1</v></r><r><n>2</n></r></e><v>+</v><r><p><v>â</v><v>b2</v></p></r><h>*</h><v>y2</v><h>*</h><v>y1</v><v>+</v><r><p><v>â</v><v>b4</v></p></r><h>*</h><v>y4</v><h>*</h><v>y1</v><t>,</t><v>b1</v><h>*</h><e><r><v>y1</v></r><r><n>2</n></r></e><v>â</v><v>b2</v><h>*</h><v>y2</v><h>*</h><v>y1</v><t>,</t><v>b2</v><h>*</h><v>y2</v><h>*</h><v>y1</v><t>,</t><r><p><v>â</v><v>b4</v></p></r><h>*</h><v>y4</v><h>*</h><v>y1</v><h>*</h><v>st</v><t>,</t><v>b0</v><h>*</h><r><p><v>y0</v><v>+</v><v>y1</v></p></r><v>+</v><n>2</n><h>*</h><v>b1</v><h>*</h><v>y1</v><v>+</v><v>b2</v><h>*</h><v>y2</v><v>+</v><v>b4</v><h>*</h><v>y4</v><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>jacobian(F(y0,y1,y2,y3,y4,y5),[y0,y1,y2,y3,y4,y5]);</line>
</editor>
</input>
<output>
<mth><lbl>(%o2) </lbl><tb roundedParens="true"><mtr><mtd><v>â</v><v>b0</v><h>*</h><v>y1</v></mtd><mtd><v>â</v><v>b0</v><h>*</h><v>y0</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>f</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y5</v></r></e></mtd></mtr><mtr><mtd><v>â</v><v>b0</v><h>*</h><v>y1</v></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>y4</v><v>â</v><v>b2</v><h>*</h><v>y2</v><v>â</v><n>4</n><h>*</h><v>b1</v><h>*</h><v>y1</v><v>â</v><v>b0</v><h>*</h><v>y0</v></mtd><mtd><v>â</v><v>b2</v><h>*</h><v>y1</v></mtd><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>y1</v></mtd><mtd><v>f</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y5</v></r></e></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>2</n><h>*</h><v>b1</v><h>*</h><v>y1</v><v>â</v><v>b2</v><h>*</h><v>y2</v></mtd><mtd><v>â</v><v>b2</v><h>*</h><v>y1</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>b2</v><h>*</h><v>y2</v></mtd><mtd><v>b2</v><h>*</h><v>y1</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>st</v><h>*</h><v>y4</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>st</v><h>*</h><v>y1</v></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><v>b0</v></mtd><mtd><n>2</n><h>*</h><v>b1</v><v>+</v><v>b0</v></mtd><mtd><v>b2</v></mtd><mtd><n>0</n></mtd><mtd><v>b4</v></mtd><mtd><n>0</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>F(y1,y2,y3,y4,y5,y6,y7,y8):=[-f*exp(-y6)-b1*y1*y2,</line>
<line>    -f*exp(-y6)-b1*y1*y2-2*b2*y2^2-b3*y3*y2-b4*y5*y2,</line>
<line>b2*y2^2-b3*y3*y2,</line>
<line>b3*y3*y2,</line>
<line>-b4*y5*y2*st,</line>
<line>b1*(y1+y2)+2*b2*y2+b3*y3+b4*y5,-f1*exp(-y6),-f2*exp(-y6)];</line>
</editor>
</input>
<output>
<mth><lbl>(%o3) </lbl><fn><r><fnm>F</fnm></r><r><p><v>y1</v><t>,</t><v>y2</v><t>,</t><v>y3</v><t>,</t><v>y4</v><t>,</t><v>y5</v><t>,</t><v>y6</v><t>,</t><v>y7</v><t>,</t><v>y8</v></p></r></fn><t>:=</t><t>[</t><r><p><v>â</v><v>f</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y6</v></p></r></fn><v>â</v><v>b1</v><h>*</h><v>y1</v><h>*</h><v>y2</v><t>,</t><r><p><v>â</v><v>f</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y6</v></p></r></fn><v>â</v><v>b1</v><h>*</h><v>y1</v><h>*</h><v>y2</v><v>+</v><r><p><v>â</v><n>2</n></p></r><h>*</h><v>b2</v><h>*</h><e><r><v>y2</v></r><r><n>2</n></r></e><v>+</v><r><p><v>â</v><v>b3</v></p></r><h>*</h><v>y3</v><h>*</h><v>y2</v><v>+</v><r><p><v>â</v><v>b4</v></p></r><h>*</h><v>y5</v><h>*</h><v>y2</v><t>,</t><v>b2</v><h>*</h><e><r><v>y2</v></r><r><n>2</n></r></e><v>â</v><v>b3</v><h>*</h><v>y3</v><h>*</h><v>y2</v><t>,</t><v>b3</v><h>*</h><v>y3</v><h>*</h><v>y2</v><t>,</t><r><p><v>â</v><v>b4</v></p></r><h>*</h><v>y5</v><h>*</h><v>y2</v><h>*</h><v>st</v><t>,</t><v>b1</v><h>*</h><r><p><v>y1</v><v>+</v><v>y2</v></p></r><v>+</v><n>2</n><h>*</h><v>b2</v><h>*</h><v>y2</v><v>+</v><v>b3</v><h>*</h><v>y3</v><v>+</v><v>b4</v><h>*</h><v>y5</v><t>,</t><r><p><v>â</v><v>f1</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y6</v></p></r></fn><t>,</t><r><p><v>â</v><v>f2</v></p></r><h>*</h><fn><r><fnm>exp</fnm></r><r><p><v>â</v><v>y6</v></p></r></fn><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>jacobian(F(y1,y2,y3,y4,y5,y6,y7,y8),[y1,y2,y3,y4,y5,y6,y7,y8]);</line>
</editor>
</input>
<output>
<mth><lbl>(%o4) </lbl><tb roundedParens="true"><mtr><mtd><v>â</v><v>b1</v><h>*</h><v>y2</v></mtd><mtd><v>â</v><v>b1</v><h>*</h><v>y1</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>f</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y6</v></r></e></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><v>â</v><v>b1</v><h>*</h><v>y2</v></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>y5</v><v>â</v><v>b3</v><h>*</h><v>y3</v><v>â</v><n>4</n><h>*</h><v>b2</v><h>*</h><v>y2</v><v>â</v><v>b1</v><h>*</h><v>y1</v></mtd><mtd><v>â</v><v>b3</v><h>*</h><v>y2</v></mtd><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>y2</v></mtd><mtd><v>f</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y6</v></r></e></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>2</n><h>*</h><v>b2</v><h>*</h><v>y2</v><v>â</v><v>b3</v><h>*</h><v>y3</v></mtd><mtd><v>â</v><v>b3</v><h>*</h><v>y2</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>b3</v><h>*</h><v>y3</v></mtd><mtd><v>b3</v><h>*</h><v>y2</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>st</v><h>*</h><v>y5</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>â</v><v>b4</v><h>*</h><v>st</v><h>*</h><v>y2</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><v>b1</v></mtd><mtd><n>2</n><h>*</h><v>b2</v><v>+</v><v>b1</v></mtd><mtd><v>b3</v></mtd><mtd><n>0</n></mtd><mtd><v>b4</v></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>f1</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y6</v></r></e></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><v>f2</v><h>*</h><e><r><s>%e</s></r><r><v>â</v><v>y6</v></r></e></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr></tb>
</mth></output>
</cell>

</wxMaximaDocument>PK      Ë¥fNñBH                       mimetypePK      Ë¥fNQdBV5  5  
             5   format.txtPK      Ë¥fN³`                   content.xmlPK      §   N$    