function [p,resid,outp] = nlfit(f,p0,lb,ub,verb)
% function [p,resid,outp] = NLFIT(f,p0,lb,ub,verb)
%
% NLFIT minimizes f

if nargin<2,
	print_usage ();
end

opt = optimset();
if nargin>2 && ~isempty(lb),
	opt = optimset(opt,'lbound',lb(:));
end
if nargin>3 && ~isempty(ub),
	opt = optimset(opt,'ubound',ub(:));
end
if nargin>4 && verb==1,
	opt = optimset(opt,'user_interaction',@iter_display);
end

[p, resid, cvg, outp] = nonlin_residmin (f, p0(:), opt);

info = residmin_stat (f, p, optimset('ret_dfdp','true'));

J = info.dfdp;
%outp.J = J;

n = length(resid);
m = numel(p);

% Calculate covariance matrix
[~,R] = qr(J,0);
Rinv = R\eye(size(R));
diag_info = sqrt(sum(Rinv.*Rinv,2));

outp.rmse = norm(resid) / sqrt(n-m);
outp.se = diag_info * outp.rmse;

alpha = 0.05;

% Calculate confidence interval
outp.dp = outp.se * tinv(1-alpha/2,n-m);

% Correlation matrix
outp.C = inv(J.'*J)./((diag_info*ones(1,m)).*((diag_info*ones(1,m)).'))*100;

endfunction

function [stop, info] = iter_display (p, vals, state)

	persistent p_ = 0;
	
	if  strcmp(state,'init')
		p_ = p;
		printf('%s\t%8s\t%8s\n','iter','f^2','dp^2');
		r = vals.residual;
		dp = 0;
		printf('%d\t%8g\t%8g\n',vals.iteration,norm(r),norm(dp));
		fflush(stdout);
	elseif strcmp(state,'iter')
		r = vals.residual;
		printf('%d\t%8g\t%8g\n',vals.iteration,norm(r),norm(p-p_));	
		fflush(stdout);
		p_ = p;
	endif
	
    stop = false;
    info = {};
    
endfunction
 