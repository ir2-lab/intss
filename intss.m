function [C,dC,RR,Cr,dCr] = intss(Ts,ts,C0,G,options)
% function [C,dC,RR,Cr,dCr] = intss(Ts,ts,C0,G,options)
%
% Integrate the Simpson & Sossin equations of diffusion annealing
% for the Id + Ie stage
%
% Input
% Ts : [nx1] annealing temperatures (K)
% ts : [nx1] annealing times (s)
% C0 : [1x5] initial defect concentration (atomic), V, I, I2, I3 and Traps
% G : [m x 3] matrix definition of initial distribution of I-V separation
%     G=[Rp1 f1 n1; Rp2 f2 n2; ...] where 
%     Rp: distribution mean 1/Rp=<1/R>, 
%     f : distribution fraction 
%     n : distribution type (n=0 for delta function or n=0.5 for the 
%         modified exponential)
% options : (optional) structure array with the following fields:
%           f0 : [1x1] pre-exponential factor (s^-1), default = 1e13
%           Em : [1x1] interstitial migration barrier (eV), default = 0.34
%           R0 : [1x1] I-V recombination radius (in lattice units), 
%                default = 3.3
%           Rt : [1x1] I-T interaction (trapping) radius (in units of R0), 
%                default = 1
%           fulldiff : [1x1 boolean] 1 for the full diffusional treatment,
%                      0 for no, default=1
%           unsaturable : [1x1 boolean] 1 for un-saturable traps,
%                      0 for saturable, default=0
%
% Output
% C   : [nx5] concentration of defects at each annealing T
% dC  : [nx5] change of defect concentration at each annealing T, 
%       dC(i)=C(i)-C(i-1)
% RR  : [nx5] reaction rate at each T
% Cr  : [nx1] vacancy concentration due to correlated recovery alone
% dCr : [nx1] variation of Cr
%
% V1:
% fo as input parameter
% set Ro=3.3a for all reaction radii
% integration variable u = sqrt(D*t)/Ro
% input a matrix G=[Ri f n; ...] where Ri initial I-V distance, fraction, 
% n=0 for delta
% and n=0.5 for mod. gaussian
%
% V2:
% improve the coding to calculate reaction rates
%
% Reactions:
%   I + V -> 0   (1) correlated
%   I + V -> 0   (2) uncorrelated
%   I + I -> I2  (3)
%   I + I2-> I3  (4)
%   I + T -> IT  (5)
%
% Species
%   V (1)
%   I (2)
%   I2(3)
%   I3(4)
%   IT(5)
%   F (6) from Simpson & Sossin, last eq.
%
% the out matrices are [n x m] where n are the # of T points
%
% V3 14/7/2014:
%
% correct a factor 2 in the dI/dt due to I+I->I2
%
% set a = 4 pi ri D [ 1 + ri/sqrt( (1+delta(1,n)) D t ) ]
%
% include a 7th species to calc total loss of V due to correlated 
%
% V4 20/10/2015
% 
% include case q=0.25 in correlated recombination
% for the piece-wise constant distribution
%
% V5 24/10/2015
%
% Changed the definition of piecewise const
%
% V6 14/11/2015
%
% - Introduced the option parameter
% - Reaction radii with SIA bias
% - Coded the jacobian
% - Separate calculation of correlated rec. is optional
%
% V7 18/12/2015
%
% - Added second modified exponential distribution for correlated rec.
%
% V8 18/1/2016
%
% - Changed integrator to lsode
% - removed the step method. the integration is performed with 1 call to lsode
%
% V9 26/1/2016
%
% - added option "fulldiff"
%
% V10 31/10/2017
%
% - added posibility to start with arbitrary initial condition C0
%   e.g., not equal V & I, nonzero I2, etc
%
% V11 7/3/2019
%
% - Modified the equations to calculate changes in trap concentration and
%   have a parameter for saturable/unsaturable traps, and a parameter option
%   for the trapping radius 
% - Solver becomes ode45 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if no options given set it to empty
if nargin<5, options=[]; end

% if options is empty set all to default
if isempty(options),
    options.fo = 1e13;
    options.Em = 0.34;
    options.R0 = 3.3;
    options.fulldiff = 1;
    options.unsaturable = 0;
    options.Rt = 1; % Rt/Ro trap radius to R0 ratio
end

% ensure default options are defined
if ~isfield(options,'fo'),
    options.fo = 1e13;
end
if ~isfield(options,'Em'),
    options.Em = 0.34;
end
if ~isfield(options,'R0'),
    options.R0 = 3.3;
end
if ~isfield(options,'fulldiff'),
    options.fulldiff = 1;
end
if ~isfield(options,'unsaturable'),
    options.unsaturable = 0;
end
if ~isfield(options,'Rt'),
    options.Rt = 1;
end

kb = 8.617e-5; % eV / K

% initial conditions
Cfp0 = C0(1); % initial # of V
Ci0 = C0(2)/Cfp0; % initial # of I (scaled)
C0 = C0/Cfp0; % scale C to Cfp0
st = 1; % st = saturable traps
if options.unsaturable,
  st = 0;
endif

% # of correlated peaks
Mc = size(G,1);
g = zeros(size(G));
g(:,1) = G(:,1)/options.R0; % gi
g(:,2) = G(:,2)./(g(:,1)-1)./g(:,1)/sqrt(pi); % fc
g(:,3) = G(:,3); % qc

% check validity of input
if ~isempty(find(g(:,1)<=1))
    error('All gi must be > 1, i.e. Rp>R0');
end
if abs(sum(G(:,2))-1)>1e-6,
    error('Sum of wi fractions must equal 1');
end
if ~isempty(find(G(:,2)>1)) || ~isempty(find(G(:,2)<0)),
    error('All wi fractions must be positive and < 1');
end

% D diffusion const
D = options.fo*exp(-options.Em./kb./Ts);

% u variable
u = sqrt(cumsum(D.*ts))/options.R0;

% gn = Rn/Ro. 
% change this for different reaction radii
%     I-V   I-I I2-I  I-T
gn = [1.00 1.07 1.11 options.Rt]'; % according to Ortiz 2008, I bias = 1.15


% B*[u; 1] = b1*u + b2 
% factor 16 comes from bcc at. vol.
B = zeros(4,2);
B(:,1) = 16*pi*(options.R0)^3*Cfp0*gn; 
if options.fulldiff,
  B(:,2) = B(:,1).*gn./sqrt(pi*[1 2 1 1]');
end
% if B(:,2)=0 then simple diffusion

% Do we need to calculate correlated recombination
% separately ?
if nargout>3,
    calc_corr = 1;
    hodefun = @(u,y) odefun_cr(u,y,g,B,Ci0,st);
    hodejac = @(u,y) odejac_cr(u,y,g,B,Ci0,st);
else
    calc_corr = 0;
    hodefun = @(u,y) odefun(u,y,g,B,Ci0,st);
    hodejac = @(u,y) odejac(u,y,g,B,Ci0,st);
end

% Initial concentrations, scaled to Cfp0
if calc_corr,
    % Vc = vacancies recombined with correlated I
    % F = rate of I reduction except correlated
    %       V   I   I2  I3 It F Vc
    C0 = [  C0(:)'  0 G(:,2)'*Ci0];
else
    % F = rate of I reduction except correlated
    %       V   I   I2  I3 It F
    C0 = [  C0(:)'  0]; 
end

% option for the ode solver
% solution must be non-negative
odeopt = odeset(...
    'NonNegative',1:size(C0,2),...
    'Jacobian',hodejac,...
    'RelTol',1e-6,...
    'AbsTol',1e-9);
    
[u,C]=ode45(hodefun,[0; u],C0,odeopt);

u(1) = [];
C(1,:) = []; % initial condition not needed

% calc output args
n = length(Ts);
m = size(C0,2);
dC=zeros(size(C));
RR=zeros(n,4+Mc);

dC(1,:) = C(1,:)-C0;
RR(1,:) = rates(u(1),C(1,:),g,B,Ci0)/(2*u(1)*(options.R0)^2/D(1));
for i=2:length(Ts),
    RR(i,:) = rates(u(i),C(i,:),g,B,Ci0)/(2*u(i)*(options.R0)^2/D(i));
    dC(i,:) = C(i,:)-C(i-1,:);
end

if calc_corr,
    Cr=C(:,7:end)*Cfp0;
    dCr=dC(:,7:end)*Cfp0;
end

C=C(:,1:5)*Cfp0;
dC = dC(:,1:5)*Cfp0;

RR = RR/Cfp0;

end % intss

%%%%%%%% sub-function definitions %%%%%%%%%%%%%%%%

% calculate derivatives, separating correlated recombination components
function dydt = odefun_cr(u,y,g,B,Ci0,st)
    dydt=zeros(size(y));
    rr = rates(u,y,g,B,Ci0);
    Mc = size(g,1);
    dydt(1) = -sum(rr(5:end)) - rr(1);
    dydt(2) = -[1 2 1 1 ones(1,Mc)]*rr;
    dydt(3) = rr(2)-rr(3);
    dydt(4) = rr(3);
    dydt(5) = -rr(4)*st;
    dydt(6) = sum((B*[u; 1]).*[y(1)+y(2) 2*y(2) y(3) y(5)]');
    dydt(7:end) = -rr(5:end);
end

% calculate derivatives
function dydt = odefun(u,y,g,B,Ci0,st)
    dydt=zeros(size(y));
    rr = rates(u,y,g,B,Ci0);
    Mc = size(g,1);
    dydt(1) = -sum(rr(5:end)) - rr(1);
    dydt(2) = -[1 2 1 1 ones(1,Mc)]*rr;
    dydt(3) = rr(2)-rr(3);
    dydt(4) = rr(3);
    dydt(5) = -rr(4)*st;
    dydt(6) = sum((B*[u; 1]).*[y(1)+y(2) 2*y(2) y(3) y(5)]');
end

% calculate the Jacobian, separating correlated recombination components
function J = odejac_cr(u,y,g,B,Ci0,st)
    Mc = size(g,1);
    J = zeros(6+Mc,6+Mc);
    b = B*[u; 1];
    cr = Ci0*dfcdu(u,g)*exp(-y(6));
    J(1,1) = -b(1)*y(2); J(1,2) = -b(1)*y(1); J(1,6) = sum(cr);     
    J(2,1) = J(1,1); J(2,2) = -b(1)*y(1)-4*b(2)*y(2)-b(3)*y(3)-b(4)*y(5);
    J(2,3) = -b(3)*y(2); J(2,5)=-b(4)*y(2); J(2,6) = J(1,6); 
    J(3,2) = 2*b(2)*y(2) - b(3)*y(3); J(3,3) = J(2,3);
    J(4,2) = b(3)*y(3); J(4,3) = b(3)*y(2);
    J(5,2) = -b(4)*y(5)*st; J(5,5)=-b(4)*y(2)*st;
    J(6,1) = b(1); J(6,2) = b(1)+2*b(2); J(6,3) = b(3); J(6,5)=b(4);
    J(7:end,6) = cr';
end

% calculate the Jacobian
function J = odejac(u,y,g,B,Ci0,st)
    J = zeros(6,6);
    b = B*[u; 1];
    cr = Ci0*dfcdu(u,g)*exp(-y(6));    
    J(1,1) = -b(1)*y(2); J(1,2) = -b(1)*y(1); J(1,6) = sum(cr);     
    J(2,1) = J(1,1); J(2,2) = -b(1)*y(1)-4*b(2)*y(2)-b(3)*y(3)-b(4)*y(5);
    J(2,3) = -b(3)*y(2); J(2,5)=-b(4)*y(2); J(2,6) = J(1,6); 
    J(3,2) = 2*b(2)*y(2) - b(3)*y(3); J(3,3) = J(2,3);
    J(4,2) = b(3)*y(3); J(4,3) = b(3)*y(2);
    J(5,2) = -b(4)*y(5)*st; J(5,5)=-b(4)*y(2)*st;
    J(6,1) = b(1); J(6,2) = b(1)+2*b(2); J(6,3) = b(3); J(6,5)=b(4);
end

% calculate reaction rates
function rr = rates(u,y,g,B,Ci0)
    Mc = size(g,1);
    rr = zeros(4+Mc,1);
    rr(1:4) = (B*[u; 1]).*[y(1) y(2) y(3) y(5)]'.*y(2);
    rr(5:end) = Ci0*dfcdu(u,g)*exp(-y(6));
end

% calculate df/dt for correlated recombination
function dfc = dfcdu(u,g)
  Mc = size(g,1);
  dfc = zeros(1,Mc);
  gi = g(:,1);
  fc = g(:,2);
  qc = g(:,3);
  if (u>0),
    for ic=1:Mc,
        x = u/(gi(ic)-1);
        if qc(ic)==0,  % delta function
            x = 1./x^2;
            dfc(ic) = fc(ic)*exp(-0.25*x).*x;
        elseif qc(ic)==0.5, % modified gaussian
            if x>10,
                dfc(ic) = fc(ic)./x.^2;
            else
                dfc(ic) = ...
                    2*fc(ic)*(1-sqrt(pi)*x.*exp(x.^2).*erfc(x));
            end
        end
    end
  end
end
