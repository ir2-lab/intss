%% intss test 2
% calc Id + Ie in isochronal annealing
% at different FP concentrations 1, 10 & 100 ppm
% with 5 ppm of traps
% 
clear
dT = 2; % temperature step
T = (60:dT:180)';
dt = 300*ones(size(T)); % isochronal, 5 min per step
N=length(T); % # of annealing steps

% initial concentration
Nfp = [1 10 100]*1e-6;
% traps
Nt = 5e-6;
M=length(Nfp);

% recovery rate
dRR = zeros(size(T,1),M);

% do the calculation and plot result
tic
for i=1:3,
  opt.unsaturable = 1;
    [C,dC] = intss(T,dt,[Nfp(i) Nfp(i) 0 0 Nt],[4 1 0.5],opt);
    dRR(:,i) = -dC(:,1)./dT/Nfp(i)*100;
end
toc

clf
plot(T,dRR)
xlabel('$T$ (K)')
ylabel('Recovery rate (\%/K)')
title('intss test no. 2')
xlim([60 180])
legend('1 ppm','10','100')
print2pdflatex(gcf,[12 9],'test2')

