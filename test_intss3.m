%% intss test 1
% calc Id + Ie in isochronal annealing
% at different FP concentrations 1, 10 & 100 ppm
% 
clear
dT = 2; % temperature step
T = (60:dT:180)';
dt = 300*ones(size(T)); % isochronal, 5 min per step
N=length(T); % # of annealing steps

% initial concentration
C0= [100 100 0 0  0 ; 100 80 7 2  0 ;  160 128 11.2 3.2  0; 100 100 20 10  0 ;100 90 5 0  0 ]*1e-6;
%Nfp = [1 10 100]*1e-6;
% no traps
%Nt = 0e-6;
M=length(C0(1,1));

% recovery rate
dRR = zeros(size(T,1),M);

% do the calculation and plot result
tic
for i=1:5,
    [C,dC] = intss(T,dt,C0(i,:),[4 1 0.5]);
    dRR(:,i) = -dC(:,1)./dT./C0(i,1).*100;
end
toc

clf
set(gcf,'paperunits','centimeters')
sz = [12 9];
set(gcf,'papersize',sz)
set(gcf,'paperposition',[0 0 sz])
plot(T,dRR)
xlabel('T (K)')

ylabel('Recovery rate (%/K)')
title('intss test no. 3-no traps')
xlim([60 180])
legend('100-100-0-0','100-80-7-2','160-128-11.2-3.2', '100-100-20-10', '100-90-5-0')
print -dpdf test3.pdf

