\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
    
%\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
    
\usepackage[round]{natbib}
\usepackage{hyperref}

%%%%%%%%%%%%%%%% Code Listing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{listings}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Matlab,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2                        % sets default tabsize to 2 spaces
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{textcomp}
\author{
  G. Apostolopoulos\\
  \texttt{gapost@ipta.demokritos.gr}
  \and
  Z. Kotsina\\
  \texttt{kotsina@ipta.demokritos.gr}
}
\date{March 2016}
\title{\texttt{intss} \\ 
Integration of Simpson-Sossin Recovery Equations in \texttt{GNU OCTAVE}
}
     
\begin{document}

\maketitle

\section{Introduction}
This document describes the numerical integration of the differential equations for diffusion annealing of defects, as given by \citet{simpson1970}. The integration has been implemented in the \texttt{OCTAVE} computing environment \citep{eaton2015} as the routine \texttt{intss}.

\section{Equations for diffusion annealing}

The theory of \citet{simpson1970} describes the evolution of defect concentrations based on the following set of reactions
\begin{subequations}
\begin{alignat}{2}
 \label{eq:iv}
 &\text{I} + \text{V} &&\to 0\\  
 &\text{I} + \text{I} &&\to \text{I}_2 \\
 &\text{I} + \text{I}_2 &&\to \text{I}_3 \\
 &\text{I} + \text{T} &&\to \text{TI} 
\end{alignat}
\end{subequations} 

The (atomic) concentration of each defect is denoted as $\rho_n$ where $n=0,1,\dots,4$ for V, I, I$_2$, I$_3$, T, respectively, and the rate of each reaction is $r_m$, $m=1\dots 4$. The evolution of defect concentrations is governed by a set of differential equations 
\begin{subequations}
\begin{align}
\dot{\rho_0} &= -r_1\\
\dot{\rho_1} &= -(r_1 + 2\, r_2 + r_3 +r_4)\\
\dot{\rho_2} &= r_2 - r_3\\
\dot{\rho_3} &= r_3\\ 
\dot{\rho_4} &= -r_4
\end{align}
\end{subequations} 
According to the work of \citet{simpson1970} and \citet{schroeder1973}, which is heavily based on the theory of Waite on diffusion limited reactions \citep{waite1957}, the reaction rates are
\begin{subequations}
\begin{align}
r_{n+1} &= \alpha_{n} \rho_{n} \rho_1, \quad n=0,1,2\\
r_{4} &= \alpha_{4} \rho_{4} \rho_1
\end{align}
\end{subequations}
where
\begin{equation}
\alpha_n = (4\pi/V_0) R_n D 
\left( 1 + R_n / \sqrt{\pi (1+\delta_{1,n}) D t} \right) 
\end{equation}
and $R_n$ is a reaction radius for the reaction between an I and the $n$-th type of defect. $V_0$ is the atomic volume and $D$ is the diffusion constant of the interstitial.

\subsection{Correlated recombination}
Extra terms must be included for the correlated recovery. The rate of correlated I-V recombination $r_{1,c}$ is
\begin{equation}
r_{1,c} = \rho_1^0 \: (df/dt) \; e^{-F} 
\end{equation}
where $\rho_1^0$ is the initial concentration of interstitials (or eq. FPs), $df/dt$ the average rate of recombination of a correlated I-V pair and $F$ is given by
\begin{equation}
\dot{F} =  \alpha_0 (\rho_0+\rho_1) + 2\alpha_1\rho_1 + \alpha_2\rho_2 + \alpha_4\rho_4
\end{equation}
It noted that $F$ is associated with the total probability that an interstitial is captured by a defect other than its own vacancy.

To include correlated recovery $r_1$ has to be replaced by $r_1 + r_{1,c}$ and the eq. for $\dot{F}$ has to be included in the set of differential equations. 

\subsubsection{Expressions for correlated recombination}
The general relation for the average probability of correlated recombination is
\begin{equation}
f(t) = \left\langle 
\frac{R_0}{R_i} \text{erfc} \left(
\frac{R_i-R_0}{\sqrt{4 D t}} 
\right) 
\right\rangle _{R_i}
\label{eq::f}
\end{equation}
where the brackets denote averaging over the initial distribution $g(R_i)$ of interstitials with respect to their distance from their vacancy $R_i$. At $t \to \infty$ the probability $f(t)$ tends to the finite value
\begin{equation}
f(\infty) \to \left\langle 
\frac{R_0}{R_i}  
\right\rangle _{R_i}
\end{equation}
Thus the fraction $1-f(\infty)$ of interstitials escape recombination with their vacancy.

The most important distributions are:

\begin{description}

\item [The delta function]
\begin{equation}
g(R_i) = \frac{\delta(Ri-R_p)}{4\pi R_p^{2}}
\end{equation}
which is normalized to
\begin{equation}
4\pi\int_{R_0}^\infty { g(R_i) R_i^2 dR_i} = 1.
\end{equation}
In this case all interstitials are initially at a distance $R_p$ from their vacancies. The average recombination probability has the same form as eq. (\ref{eq::f}) with $R_i$ replaced by $R_p$ and $f(\infty)=R_0/R_p$.

\item [The modified exponential] 
\begin{equation}
g(R_i) = \frac{1}{4\pi R_p (R_p-R_0)} \frac{e^{-(R_i-R_0)/(R_p-R_0)}}{R_i}
\end{equation}
again normalized as above.
The parameter $R_p$ is defined so that
\begin{equation}
f(\infty) =\left\langle R_0 / R_i \right\rangle = R_0 / R_p 
\end{equation}
The recombination probability is
\begin{equation}
f(t) = (R_0 / R_p) \left[  1 - \exp\frac{Dt}{(R_p-R_0)^2} \;
\text{erfc} \frac{\sqrt{Dt}}{R_p-R_0}
 \right] 
\end{equation}

\end{description}



\section{Numerical solution}

For numerical solution the equations are transformed according to the following rules:
\begin{itemize}
\item The concentrations are scaled to the initial FP concentration. The new variables $y_n$=$\rho_n / \rho_1^0$ are introduced.
\item A new independent variable is introduced 
\begin{equation}
u = \sqrt{D t}/R_0.
\end{equation}
\end{itemize}
The equations for the $y_n(u)$ are obtained by the following transformation
\begin{equation}
\frac{dy_n}{du} = \frac{1}{\rho_1^0}\frac{d\rho_n}{dt}\frac{dt}{du}=\frac{2uR_0^2}{D\rho_1^0}\frac{d\rho_n}{dt}
\end{equation}
The reactions rates become $r'_n = (dt/du) r_n/\rho_1^0 =\beta_n y_n y_1$
where the transformed rate constants $\beta_n$ are
\begin{equation}
\label{eq:beta}
\begin{aligned}
\beta_n &= \frac{2uR_0^2\rho_1^0}{D} \alpha_n = \beta_{1,n} u + \beta_{2,n}\\ 
\beta_{1,n} &= \frac{8\pi R_0^3}{V_0} \gamma_n \rho_1^0; \quad 
\beta_{2,n} =  \frac{\beta_{1,n} \gamma_n} { \sqrt{\pi (1+\delta_{1,n}) }}\\
\gamma_n &= R_n / R_0 
\end{aligned}
\end{equation}
From the last equation it is evident that the transformed rate constants are temperature independent. Thus the form of the equations are the same in each annealing step.

\subsection{Correlated recombination}
For the correlated recombination, the rate is transformed to
\begin{equation}
r'_{1,c}=\frac{1}{\rho_1^0}\frac{dt}{du} \: r_{1,c} = (df/du) \; e^{-F} 
\end{equation}
Regarding $F$ as the fifth component of $y$, i.e., $F=y_5$, it is obtained that
\begin{equation}
\frac{dF}{du} = \dot{y}_5 = \beta_0 (y_0+y_1) + 2\beta_1 y_1 + \beta_2 y_2 + \beta_4 y_4
\end{equation}

For the most important distributions $g(R_i)$ the expressions for $df/du$ are given below.
\begin{description}


\item [Delta function]
\begin{equation}
\begin{aligned}
\gamma_p &= R_p/R_0; \quad x=u/(\gamma_p-1) \\
f(u) &= (\gamma_p)^{-1} \text{erfc} (1/2x)\\
\frac{df}{du} &= 
\frac{1}{\sqrt{\pi}\gamma_p(\gamma_p-1)}
\frac{e^{-1/4x^2}}{x^2}
\end{aligned}
\end{equation}


\item [Modified exponential]
\begin{equation}
\begin{aligned}
\gamma_p &= R_p/R_0; \quad x=u/(\gamma_p-1) \\
f(u) &= (\gamma_p)^{-1}
\left[ 
1 - e^{x^2} \: \text{erfc}(x) 
\right] \\
\frac{df}{du} &= 
\frac{2}{\sqrt{\pi}\gamma_p(\gamma_p-1)}
\left[ 1 -
\sqrt{\pi} x e^{x^2} \text{erfc}(x)
\right]; 
\end{aligned}
\end{equation}
For $x>20$ the following approximation is used
\begin{equation}
\frac{df}{du}\approx
\frac{1}{\sqrt{\pi}\gamma_p(\gamma_p-1)}
\left[ x^{-2} + O(x^{-4})\right] 
\end{equation}

\end{description}

\subsection{Equation system}
We rewrite here for clarity the full ODE system
\begin{subequations}
\label{eq:sysy}
\begin{align}
\dot{y}_0 &= -\dot{f} e^{-y_5} - \beta_0 y_0 y_1 \\
\dot{y}_1 &= \dot{y}_0 - 2 \beta_1 y_1 y_1 -  \beta_2 y_2 y_1 -\beta_4 y_4 y_1\\
\dot{y}_2 &= \beta_1 y_1 y_1 - \beta_2 y_2 y_1 \\
\dot{y}_3 &= \beta_2 y_2 y_1 \\
\dot{y}_4 &= -\beta_4 y_4 y_1 s_T\\
\dot{y}_5 &= \beta_0 (y_0+y_1) + 2\beta_1 y_1 + \beta_2 y_2 + \beta_4 y_4
\end{align}
\end{subequations}
The parameter $s_T$ is inserted in eq. for $y_4$ to account for un-saturable traps. If $s_T=0$ then the concentration of interstitial traps remains constant whereas if $s_T=1$ it is reduced when trapping occurs.

For efficient numerical integration the use of the $6\times 6$ Jacobian matrix
\[
J_{ij} = \frac{\partial \dot{y}_i}{\partial y_j}
\]
is recommended. For the above system the non-zero elements are
\begin{subequations}
\begin{align}
J_{00} &= J_{10} = -\beta_0 y_1, 
\quad J_{01} = -\beta_0 y_0, 
\quad J_{05} = J_{15} = \dot{f} e^{-y_5} \\
J_{11} &= -\beta_0 y_0 - 4 \beta_1 y_1 - \beta_2 y_2 -beta_4 y_4, 
\quad J_{12} = J_{22} = -\beta_2 y_1 \\
J_{21} &= 2 \beta_1 y_1 - \beta_2 y_2 \\
J_{31} &= \beta_2 y_2, \quad J_{32} = \beta_2 y_1 \\
J_{41} &= -\beta_4 y_4 s_T, \quad J_{44}=-\beta_4 y_1 s_T \\
J_{50} &=\beta_0, \quad J_{51} = \beta_0+2\beta_1, \quad J_{52} = \beta_2, \quad J_{54} = \beta_4
\end{align}
\end{subequations}

The system is typically solved with initial condition at $u=0$ ($t=0$)
\begin{equation}
\begin{aligned}
y_0 &= y_1 = 1\\
y_2 &= y_3 = y_4 = y_5 = 0
\end{aligned}
\end{equation}

\subsection{Solution for an annealing program}
A specific annealing program is given as a series of temperature-time interval couples, $\left\lbrace T_i, \Delta t_i\right\rbrace $, $i=1,2,\dots,N$. This is transformed to a series of $u$ values
\begin{equation}
u_i^2 = R_0^{-2} \sum_{j=1}^i { D_j \Delta t_j } = 
(D_0/R_0^2) \sum_{j=1}^i { e^{-E_m/k_B T_i } \Delta t_j }
\end{equation}
From the definition of $\beta_n$ in \eqref{eq:beta} it is seen that these  coefficients do not depend explicitly on temperature. Thus the equation system \eqref{eq:sysy} is valid in all annealing steps  
The integration is performed from $u_0=0$ to $u_N$ and the value of the solution is obtained in all intermediate steps $u_i$. 


\section{Implementation in \texttt{OCTAVE}}

For the integration of the 1st order differential equations the solver \texttt{ode45} has been used. A special option is passed to the solver that
the solution is always positive.  

The routine \texttt{intss} given below performs the integration of the equations for a given annealing program.

\lstinputlisting{intss.m}

\section{Tests}

Two tests are performed:
\begin{enumerate}
\item The recovery rate during isochronal annealing of Fe is calculated in the temperature range 60 to 180~K. The sub-stages I$_D$ and I$_E$ are observed. The calculation is performed at 3 different initial concentrations of Frenkel pairs.

\lstinputlisting{test_intss1.m}


\begin{center}
\includegraphics[scale=1]{test1.pdf}
\end{center}

\item The same calculation as above is performed with 5 ppm of interstitial traps 

\lstinputlisting{test_intss2.m}


\begin{center}
\includegraphics[scale=1]{test2.pdf}
\end{center}

\end{enumerate}

\bibliographystyle{plainnat}
\bibliography{references}

\end{document}
 