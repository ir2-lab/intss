%% Fit Takaki data globally
% Only Ic - Id - Ie
clear all
pkg load optim
pkg load specfun


load TakakiIcIdIe.dat

Data = TakakiIcIdIe.Data;

for id=1:4, 
    T = Data(id).T;
    R = Data(id).R;
    Data(id).dt = ones(size(T))*300;
end

% fit
fitobj = ssFitObj();

fitobj.fit(Data);

fitobj.save('fit.dat')