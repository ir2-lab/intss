classdef ssFitObj < handle

properties (Constant = true)
	% parameter names
	p_names = {'fc','fd','log(Do/Ro^2)',...
		'Rp1/Ro','Rp2/Ro','w1','log(kc)','Tc'};
		
	% par. formating
	p_fmt = {'%5.3f', '%5.3f', '%5.2f', '%5.3f', '%5.3f', '%5.3f', ...
	'%5.1f', '%5.1f'};
endproperties

properties
	% Constants
	R0 = 3.3; % I-V rec. radius
	Rfp = 3; % FP resistivity nOhm-cm/ppm
	
	% Fitting parameters
	p = []; 
	% errors
	dp = [];
	% correlation matrix
	Cp = [];

	Ec = 0.1; % eV
	
	% Data
	Data = struct();
	
	% other parameters
	N70 = zeros(4,1);
	R70 = zeros(4,1);
	T = [];
	dt = [];
	dT = [];
	Scp = [];
	Sde = [];
	Rf = [];
	
	% buffers
	Zf = [];
	idx = [];
	zd = [];
	buf = [];
	
	%% fit stat
	lsqStat = struct();

endproperties

methods

	function fit(this,Data)

		this.Data = Data;
		N = zeros(4,1);

		for id=1:4,
			N(id)=size(Data(id).T,1);
			this.N70(id)=Data(id).R(1)/this.Rfp;
			this.R70(id)=Data(id).R(1);
		end
		
		Nm = max(N);
		im = find(N==Nm);
		this.T = Data(im).T;
		this.dT = diff(Data(im).T)*ones(1,4);
		this.dt = Data(im).dt;
		this.Scp = zeros(Nm,1);
		this.Sde = zeros(Nm,4);
		this.Rf = zeros(Nm,4);
		
		Zd = zeros(Nm-1,4);
		Z0 = zeros(Nm-1,4);
		this.Zf = zeros(Nm-1,4);
		this.buf = zeros(Nm,5);

		for id=1:4,
			j=1:N(id)-1;
			Zd(j,id) = diff(Data(id).R)./diff(Data(id).T)/Data(id).R(1);
			Z0(j,id) = 1;
		end
		this.idx = find(Z0==1);
		this.zd = Zd(this.idx);

		f = @(p) chi2fun(this,p); 
		
		if isempty(this.p)
			this.p = this.getDefaultParameters();
		end
		bnds = this.getParameterBounds();
		
		this.lsqStat = struct();
		this.lsqStat.nfcall = 0;
			
		[this.p,resid,outp] = nlfit(f,this.p,bnds(:,1),bnds(:,2),1);
		
		this.lsqStat.niter = outp.niter;
		this.lsqStat.rmse = outp.rmse;
		this.lsqStat.se = outp.se;
		this.lsqStat.resid = resid;
		this.dp = outp.dp;
		this.Cp = outp.C;
			
	endfunction
	
	function save(this,fname)
		ssFitObjData = struct();
		
		ssFitObjData.R0   = this.R0   ;
		ssFitObjData.Rfp  = this.Rfp  ;
		ssFitObjData.p    = this.p    ;
		ssFitObjData.dp   = this.dp   ;
		ssFitObjData.Cp   = this.Cp   ;
		ssFitObjData.Data = this.Data ;
		ssFitObjData.N70  = this.N70  ;
		ssFitObjData.R70  = this.R70  ;
		ssFitObjData.T    = this.T    ;
		ssFitObjData.dt   = this.dt   ;
		ssFitObjData.dT   = this.dT   ;
		ssFitObjData.Scp  = this.Scp  ;
		ssFitObjData.Sde  = this.Sde  ;
		ssFitObjData.Rf   = this.Rf   ;
		
		ssFitObjData.lsqStat = this.lsqStat;
		
		save(fname,"ssFitObjData");
		
	endfunction
	
	function load(this,fname)
		load(fname);
		this.R0   = ssFitObjData.R0   ;
		this.Rfp  = ssFitObjData.Rfp  ;
		this.p    = ssFitObjData.p    ;
		this.dp   = ssFitObjData.dp   ;
		this.Cp   = ssFitObjData.Cp   ;
		this.Data = ssFitObjData.Data ;
		this.N70  = ssFitObjData.N70  ;
		this.R70  = ssFitObjData.R70  ;
		this.T    = ssFitObjData.T    ;
		this.dt   = ssFitObjData.dt   ;
		this.dT   = ssFitObjData.dT   ;
		this.Scp  = ssFitObjData.Scp  ;
		this.Sde  = ssFitObjData.Sde  ;
		this.Rf   = ssFitObjData.Rf   ;
		this.lsqStat = ssFitObjData.lsqStat;
		
	endfunction
	
	function plot(this)
		clf
		clr = get(gca,'colororder');
		clf

		Y = (3:-1:0);

		xf = this.T(2:end);
		yf = -diff(this.Rf)./this.dT*100;
		for id=4:-1:1,
			x=this.Data(id).T;
			yd = this.Data(id).R/Data(id).R(1)*100;
    
			plot(x(2:end),-diff(yd)./diff(x)+Y(id),'.',...
				xf,yf(:,id)+Y(id))
			if id==4, hold on; end
		end
		hold off
	endfunction
	
	function display(this)
		if isempty(this.dp)
			error("ssFitObj: no fitting has been performed")
		end
				
		printf("Fitted parameters:\n")
		for i=1:numel(this.p),	
			printf(['%12s = ' this.p_fmt{i} ' +/- ' this.p_fmt{i} '\n'],...
				this.p_names{i},this.p(i),this.dp(i));
		end
		
	endfunction
	
	function par = getDefaultParameters(this)
		% Fitting parameters
		par = [0.1; 0.9; ... % fc, fd, ratios of C and D pairs
			13; ... % log10(fo), jump frecuency s^-1
			1.1; 1.7; 0.5; ... % Rp1/Ro,Rp2/Ro, w1
			8; 90]; % log10(kc), Tc (K)
	endfunction
	
	function Gc = getGc(this)
		p=this.p;
		Rp1=p(4)*this.R0; Rp2=p(5)*this.R0; w1 = p(6);
		Gc = [Rp1 w1   0.5; ...
			  Rp2 1-w1 0.5];
	endfunction
	
endmethods

methods (Access = private)

	function bounds = getParameterBounds(this)
		npar = 8;
		bounds = zeros(8,2);
		bounds = [ ...
			0 Inf;
			0 Inf;
			8 15;
			1 Inf;
			1 Inf;
			0 1;
			2 13;
			85 95];
	endfunction
	
	% fitting function returns y(obs) - y(model)
	function z=chi2fun(this,p)
		this.model(p);
		this.Zf = diff(this.Rf)./this.dT;
		z = this.zd-this.Zf(this.idx);    
	end

	% calc the peaks and return the sum
	function model(this,p)
	
		this.lsqStat.nfcall = this.lsqStat.nfcall + 1;
			
		fc=p(1); fd=p(2);
		fo = 10^p(3);
		Rp1=p(4)*this.R0; Rp2=p(5)*this.R0; w1 = p(6);
		kc = 10^p(7); Tc = p(8);
		
		% calc close-pair Ic
		[this.Scp] = ...
			rec_peak_shape(this.T,this.dt,Tc,kc,1,1);
		this.Scp = this.Scp*fc;
		
		% calc corr+uncorr recombination
		intss_options.fo = fo;
		intss_options.R0 = this.R0;
		Gc = [Rp1 w1   0.5; ...
			  Rp2 1-w1 0.5];
		
		for id=1:4,
			nd70 = this.N70(id)*fd;
			this.buf = intss(this.T,this.dt,nd70*1e-6,0,Gc, intss_options);
			this.Sde(:,id) = this.buf(:,1)*fd;
			% calc the sum of peaks
			this.Rf(:,id) = this.Sde(:,id) + this.Scp;
		end
	end

endmethods

endclassdef